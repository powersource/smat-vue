# smat-vue

This is a prototype of a Vue interface for https://gitlab.com/smat-project/smat-be/-/blob/master/DOCS.md

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

Go the the address it tells you in the browser you'll see the app!

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## NOTES
Special thanks to [Protozoa Co-op](http://protozoa.nz/) for getting us started and showing us the ropes!

Here's what I did so far
- used vue-cli scaffold : https://cli.vuejs.org/guide/
- ran through cli setup, picking some options
- read up on vue forms : https://vuejs.org/v2/guide/forms.html
- vue-cli guide : https://appdividend.com/2018/02/09/vue-cli-tutorial-2018-example-scratch/
